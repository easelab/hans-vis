# Help Annotating Software“HAnS” - Visualisation #

## Requirements

* IntelliJ version 2020.3.2, 2020.3.3 or 2020.3.4
* The Plugin - Download here: https://bitbucket.org/easelab/hans-vis/downloads/hans-vis-1.0.zip

## Install ##

To install HAnS - Vis you have to install it like an IntelliJ plugin. Possible helpful guide: https://www.jetbrains.com/help/idea/managing-plugins.html#required-plugins

First, click on "IDE and Project Settings" in the Toolbar and select "Plugin": 

![Image of Settings](https://bitbucket.org/easelab/hans-vis/raw/6eb17c92fffe7de5dcc0edb268802d445cf0a6b1/guide/Settings.png)

Then navigate to "Manage Repositories, Configure Proxy or Install Plugin from Disk" and choose "Install plugin from Disk". 

![Image of Install](https://bitbucket.org/easelab/hans-vis/raw/6eb17c92fffe7de5dcc0edb268802d445cf0a6b1/guide/InstallPlugin.png)

From there navigate to the Plugin zip-file and install, then restart the IDE! :)

## How to build ##

In order to build the project jar, open the 'Gradle' tab in IntelliJ and select 'buildPlugin' option.

![Image of Building](https://bitbucket.org/easelab/hans-vis/raw/38ec15e4506a5b6aa0a3b4cc6989e8cc83af821b/guide/build.png)


## Example DataSet ##

You can try the plugin by cloning down this repo : https://github.com/johmara/Snake
The repo is forked from https://github.com/hexadeciman and has been expanded for a user study on HAnS-text which is a plugin for helping annotating software.

The user study and the plugin HAnS-text is made and developed by Johan Martinson and Herman Jansson