// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package se.ch.hans.toolwindow;

import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

/**
 * A class creating the content of the MetricWindow
 */
public class MetricWindowFactory implements ToolWindowFactory {

    /**
     * Adds the metric table to the ToolWindow content
     * @param project The project open in the IDE
     * @param toolWindow The ToolWindow the table is put on
     */
    @Override
    public void createToolWindowContent(@NotNull Project project, ToolWindow toolWindow) {
        MetricViewerWindow metricViewerWindow = ServiceManager.getService(project,
                MetricViewerWindowService.class).metricViewerWindow;
        JComponent component = toolWindow.getComponent();
        component.getParent().add(metricViewerWindow.content(), 0);
    }
}
