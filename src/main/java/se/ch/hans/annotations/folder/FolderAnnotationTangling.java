// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package se.ch.hans.annotations.folder;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public final class FolderAnnotationTangling {
    private FolderAnnotationTangling(){}

    public static Map<String, ArrayList<ArrayList<String>>> getFolderTanglings(String path) throws IOException {
        return getFeatureFolderTanglings(path + "/src");
    }

    private static Map<String, ArrayList<ArrayList<String>>> getFeatureFolderTanglings(String path) throws IOException {
        Map<String, ArrayList<ArrayList<String>>> featureFolderTangling = new HashMap<>();
        Set<String> featureFolderPaths = getFeatureFolderLocations(path);

         /*
            Get folder annotations from each .feature-to-folder file.
        */
        for (String featureFolderPath : featureFolderPaths) {

            File featureFolderFile = new File(featureFolderPath);

            /*
                Read the file to extract the feature annotated in the file.
             */
            FileReader fileReader = new FileReader(featureFolderFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;

            // All features in a given file.
            ArrayList<String> allFeatures = new ArrayList<>();

            while ((line = bufferedReader.readLine()) != null) {
                // Features annotated on a single line.
                String[] features = line.split(" ");
                Collections.addAll(allFeatures, features);
            }

            if (allFeatures.size() > 1) {
                for (String feature : allFeatures) {
                    if (!featureFolderTangling.containsKey(feature)) {
                        featureFolderTangling.put(feature, new ArrayList<>());
                    }
                    for (String tangledFeature : allFeatures) {
                        if (!feature.equals(tangledFeature)) {
                            boolean featureAlreadyExist = false;

                            for (ArrayList<String> featureTanglings : featureFolderTangling.get(feature)) {
                                if (featureTanglings.get(0).equals(tangledFeature)) {
                                    int tanglingDegree = Integer.parseInt(featureTanglings.get(1));
                                    tanglingDegree++;
                                    featureTanglings.set(1, String.valueOf(tanglingDegree));
                                    featureAlreadyExist = true;
                                }
                            }
                            if (!featureAlreadyExist) {
                                ArrayList<String> data = new ArrayList<>();
                                data.add(tangledFeature);
                                data.add("1");
                                featureFolderTangling.get(feature).add(data);
                            }
                        }
                    }
                }
            }
        }
        return  featureFolderTangling;
    }

    /**
     * Helper method to get where all the .feature-to-folder files are located.
     *
     * @param path - path to project root.
     * @return Set containing the file paths to every .feature-to-folder file.
     */
    private static Set<String> getFeatureFolderLocations(String path) {
        Set<String> featureFolderPaths = new HashSet<>();
        File root = new File(path);
        String filename = ".feature-to-folder";
        try {

            Collection<File> files = FileUtils.listFiles(root, null, true);

            for (File file : files) {
                if (file.getName().equals(filename))
                    featureFolderPaths.add(file.getAbsolutePath());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return featureFolderPaths;
    }
}
