// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package se.ch.hans.misc;

import org.jetbrains.annotations.Nls;

/**
 * A class representing a feature
 */
public class Feature {
    /**
     * Enum of metrics:
     * - SD (Scattering Degree)
     * - NOCA (Number Of Code Annotations)
     * - NOFIA (Number Of File Annotations)
     * - NOFOA (Number Of Folder Annotations)
     * - LOFC (Lines Of Feature Code)
     */
    public enum Metric {
        SD("SD"),
        NOCA("NoCA"),
        NOFIA("NoFiA"),
        NOFOA("NoFoA"),
        TD("TD"),
        LOFC("LOFC");

        private final String displayName;
        Metric(String displayName) { this.displayName = displayName; }

        /**
         * Returns the name to be displayed for the metric
         * @return The display name
         */
        @Nls(capitalization = Nls.Capitalization.Title)
        public String getDisplayName() {
            return displayName;
        }
    }

    public static final int NUM_METRICS = Metric.values().length;
    // Metrics
    public int sd;
    public int noca;
    public int nofia;
    public int nofoa;
    public int td;
    public int lofc;
    // Feature name
    public String name;

    /**
     * Creates a new feature with all metrics set to 0
     * @param name The name of the feature
     */
    public Feature(String name) {
        this(name,0,0,0,0,0,0);
    }

    /**
     * Creates new feature with metric according to parameters
     * @see Metric
     * @param name The name of the feature
     * @param sd The SD metric for the feature
     * @param noca The NoCA metric for the feature
     * @param nofia The NoFiA metric for the feature
     * @param nofoa The NoFoA metric for the feature
     * @param td The TD metric for the feature
     * @param lofc The LoFC metric for the feature
     */
    public Feature(String name, int sd, int noca, int nofia, int nofoa, int td, int lofc) {
        this.name = name;
        this.sd = sd;
        this.noca = noca;
        this.nofia = nofia;
        this.nofoa = nofoa;
        this.td = td;
        this.lofc = lofc;
    }
}
